import axios from 'axios';

const API_KEY = 'fd7d02ea651bd29033d8f625c565bc42';
const rootURL = `http://api.openweathermap.org/data/2.5/forecast?appid=${API_KEY}`;

export const FETCH_WEATHER = 'FETCH_WEATHER';

export function fetchWeather (city) {
  if (!city) {
    return {
      type: FETCH_WEATHER,
      payload: {}
    }
  }
  const url = `${rootURL}&q=${city},india`;
  const request = axios.get(url);

  // payload is optional property
  return {
    type: FETCH_WEATHER,
    payload: request
  }
}
