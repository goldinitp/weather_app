import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchWeather } from '../actions/index';

class SearchPage extends Component {
    constructor(props) {
        super(props);

        this.state = {term: ''};
        this.onFormSubmit = this.onFormSubmit.bind(this);
        this.onInputChange = this.onInputChange.bind(this);
    }

    onInputChange(event) {
      this.setState({term: event.target.value});
    }

    onFormSubmit(event) {
      event.preventDefault();
      // need to fetch weather data here
      this.props.fetchWeather(this.state.term);
      this.setState({term: ''});
    }

    render() {
        return (
            <form className="input-group" onClick={this.onFormSubmit}>
                <input
                  placeholder="Get a five-day forecast in your favorite cities"
                  className="form-control"
                  value={this.state.term}
                  onChange={this.onInputChange} />
                <span className="input-group-btn">
                  <button type="submit" className="btn btn-secondary">Submit</button>
                </span>
            </form>
        );
    }
}

// function mapStateToProps (state) {
//   return null;
// }

function mapDispatchToProps (dispatch) {
  return bindActionCreators({fetchWeather: fetchWeather}, dispatch);
}

export default connect(null, mapDispatchToProps)(SearchPage);
