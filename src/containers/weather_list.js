import React, { Component } from 'react';
import {connect} from 'react-redux';
import { Sparklines, SparklinesLine } from 'react-sparklines';
import GoogleMap from '../components/google_map';
import Chart from '../components/chart';

// alert
// need to see why c should be capital in chart
class WeatherList extends Component {
    constructor(props) {
        super(props);
    }

    renderCity(cityData) {
        const cityname = cityData.city.name;
        const temps = cityData.list.map(listData => listData.main.temp);
        const pressures = cityData.list.map(listData => listData.main.pressure);
        const humidities = cityData.list.map(listData => listData.main.humidity);
        const latitude = cityData.city.coord.lat;
        const longitude = cityData.city.coord.lon;

        return (
          <tr key={cityname} className="main-tr">
              <td className="google-map-wrapper"><GoogleMap lat={latitude} lon={longitude}/></td>
              <td>
                <Chart data={temps} color="orange" />
              </td>
              <td>
                <Chart data={pressures} color="blue" />
              </td>
              <td>
                <Chart data={humidities} color="green" />
              </td>
          </tr>
        );
    }

    render() {
        if (!this.props.weather.length) {
          return <div className="no-weather-msg"> Please search for your city </div>;
        }
        return (
            <table className="table table-hover">
                <thead>
                  <tr>
                    <td>City</td>
                    <td>Temperature</td>
                    <td>Pressure</td>
                    <td>Humidity</td>
                  </tr>
                </thead>
                <tbody>
                  {this.props.weather.map(this.renderCity)}
                </tbody>
            </table>
        );
    }
}

function mapStateToProps (state) {
  return {
    weather: state.weather
  }
}

export default connect(mapStateToProps)(WeatherList);
