import { FETCH_WEATHER } from '../actions/index';

export default function weatherReducer(state = [], action) {
  if (action.payload && !action.payload.data) {
    return state;
  }
  switch(action.type) {
    case FETCH_WEATHER:
      return [action.payload.data, ...state];
      // immutable state
      // dont use state.push
      // state.concat can be used since it returns a new array
  }
  return state;
}
